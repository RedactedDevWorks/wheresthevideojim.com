title: Metocast Announcement
date: 2015-12-18 00:00:00
author: RedactedProfile
thumbnail: http://img.youtube.com/vi/164ox8X7WBQ/hqdefault.jpg
category: Video
template: video
series: Metocast
persona: MisterMetokur
file: 
youtube: 164ox8X7WBQ
soundcloud: 
vimeo: 
dailymotion: 
magnet: 
torrent: 
url: video/mistermetokur/metocast/metocast-announcement
save_as: video/mistermetokur/metocast/metocast-announcement/index.html
summary: 

<div>I'll be hosting a political podcast on a weekly basis starting this weekend. It will run from 12 noon till 3 pm central standard time each Saturday. For this first episode we'll be discussing Bernie Sanders and his recent snafu with the Hillary campaign as well as Jeb Bush and his inability to buy popularity. We'll also cover the recent Republican Presidential debate and any political happenings of the last few weeks. Kyle Gannon will be coming on to pitch his candidate Bernie Sanders to me and explain why I just don't get it.<br />
<br />
A link will be posted before the show to the stream. Hope to see you there.</div>

