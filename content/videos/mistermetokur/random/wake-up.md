title: Wake Up
date: 2015-10-22 00:00:00
author: kapparino
thumbnail: http://img.youtube.com/vi/N4hFcjnOHnk/hqdefault.jpg
category: Video
template: video
series: Random
persona: MisterMetokur
file: 
youtube: N4hFcjnOHnk
soundcloud: 
vimeo: 
dailymotion: 
magnet: 
torrent: 
url: video/mistermetokur/random/wake-up
save_as: video/mistermetokur/random/wake-up/index.html
summary: 

<p>Years from now you'll be telling your children that the internet used to be a place of free speech, free expression, and free thought. Of course after you explain that you'll be put into a camp for telling stories of the "before time".</p>

