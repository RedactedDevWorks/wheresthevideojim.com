title: Milwaukee: The Fight Against Global Warming
date: 2016-08-14 00:00:00
author: kapparino
thumbnail: 
category: Video
template: video
series: Random
persona: MisterMetokur
file: 
youtube: 
soundcloud: 
vimeo: 
dailymotion: 
magnet: 
torrent: 
url: video/mistermetokur/random/milwaukee-the-fight-against-global-warming
save_as: video/mistermetokur/random/milwaukee-the-fight-against-global-warming/index.html
summary: 

<p>Activists take the fight to fossil fuels in the hopes of freeing a city after a young man's death motivates them to make a change.</p>

