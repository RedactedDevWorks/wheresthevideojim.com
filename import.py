import sqlite3, sys, shutil, datetime, time, os
from slugify import slugify


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

conn = sqlite3.connect('wtvj.db')
conn.row_factory = dict_factory
c = conn.cursor()


posts = []

c.execute(' SELECT '
          ' node.nid as id, '
          ' node.title as title, '
          ' node.created as date_added, '
          ' node.changed as date_updated, '
          ' upload_date.field_original_upload_date_value as date_uploaded, '
          ' body.body_value as content, '
          ' body.body_summary as summary, '
          ' users.name as author '
          ' FROM node '
          ' LEFT JOIN field_data_body as body ON node.nid = body.entity_id '
          ' LEFT JOIN field_data_field_original_upload_date as upload_date ON node.nid = upload_date.entity_id '
          ' LEFT JOIN users ON node.uid = users.uid '
          ' WHERE '
          ' node.type == "video" ')

video_posts = c.fetchall()

for post in video_posts:

    # taxonomy data
    c.execute(" select "
              "   taxonomy_vocabulary.name as `key`, "
              "   taxonomy_term_data.name as `value` "
              " from taxonomy_index "
              " left join taxonomy_term_data on taxonomy_term_data.tid = taxonomy_index.tid "
              " left join taxonomy_vocabulary on taxonomy_term_data.vid = taxonomy_vocabulary.vid "
              " where "
              "   taxonomy_index.nid = %d" % (post['id']))
    taxonomy_data = c.fetchall()
    series = None
    persona = None
    for taxomony in taxonomy_data:
        if taxomony['key'] == "Series":
            series = taxomony['value']
        if taxomony['key'] == "Personas":
            persona = taxomony['value']

    # video data
    c.execute(" select "
              "   file_managed.filename, "
              "   file_managed.uri, "
              "   file_managed.filemime "
              " from "
              "   field_data_field_videos "
              " left join "
              "   file_managed on file_managed.fid = field_data_field_videos.field_videos_fid "
              " where "
              "   field_data_field_videos.entity_id = %d" % (post['id']))

    videos = c.fetchall()

    # ok I think we have all the data now.
    # post
    # series
    # persona
    # videos
    posts.append({
        "id": post['id'],
        "title": post['title'],
        "author": post['author'],
        "date_added": post['date_added'],
        "date_updated": post['date_updated'],
        "date_uploaded": post['date_uploaded'],
        "content": post['content'],
        "summary": post['summary'],
        "series": series,
        "persona": persona,
        "videos": videos
    })

conn.close()

shutil.rmtree("content/videos", ignore_errors=True)

template = '''title: {TITLE}
date: {DATE_UPLOADED}
author: {AUTHOR}
thumbnail: {THUMBNAIL}
category: Video
template: video
series: {SERIES}
persona: {PERSONA}
file: {FILE}
youtube: {YOUTUBE}
soundcloud: {SOUNDCLOUD}
vimeo: {VIMEO}
dailymotion: {DAILYMOTION}
magnet: {MAGNET}
torrent: {TORRENT}
url: video/{PERSONA_SLUG}/{SERIES_SLUG}/{SLUG}
save_as: video/{PERSONA_SLUG}/{SERIES_SLUG}/{SLUG}/index.html
summary: {SUMMARY}

{CONTENT}
'''

for post in posts:
    print("Writing Post: %s \n" % post['title'])

    file = ''
    youtube = ''
    vimeo = ''
    dailymotion = ''
    magnet = ''
    torrent = ''
    soundcloud = ''
    thumbnail = ''

    for video in post['videos']:
        if video['filemime'] == "video/youtube":
            youtube = video['uri'].replace("youtube://v/", "")
            if youtube:
                thumbnail = "http://img.youtube.com/vi/%s/hqdefault.jpg" % youtube
        elif video['filemime'] == "audio/soundcloud":
            soundcloud = video['uri'].replace("soundcloud://u/", "")
        elif video['filemime'] == "video/mp4":
            file = video['uri'].replace("public://", "")

    content = template.format(
        TITLE=post['title'],
        AUTHOR=post['author'],
        DATE_ADDED=datetime.datetime.fromtimestamp(post['date_added']).strftime("%Y-%m-%d"),
        DATE_UPLOADED=post['date_uploaded'],
        SERIES=post['series'],
        PERSONA=post['persona'],
        THUMBNAIL=thumbnail,
        FILE=file,
        YOUTUBE=youtube,
        VIMEO=vimeo,
        DAILYMOTION=dailymotion,
        SOUNDCLOUD=soundcloud,
        MAGNET=magnet,
        TORRENT=torrent,
        PERSONA_SLUG=slugify(post['persona']),
        SERIES_SLUG=slugify(post['series']),
        SLUG=slugify(post['title']),
        SUMMARY=post['summary'],
        CONTENT=post['content']
    )

    folder = "content/videos/{Persona}/{Series}".format(
        Persona=slugify(post['persona']),
        Series=slugify(post['series'])
    )
    path = folder + "/{Slug}.md".format(
        Slug=slugify(post['title'])
    )

    # file = open("video")
    if not os.path.exists(folder):
        os.makedirs(folder, exist_ok=True)

    h = open(path, 'w')
    h.write(content)
    h.close()

print("~FIN~")
